from bonus_system import calculateBonuses

standard_program = "Standard"
premium_program = "Premium"
diamond_program = "Diamond"
no_program = ""
nothing_program = "nothing"


def test_standard():
    assert calculateBonuses(standard_program, 0) == mul(1, 0.5)
    assert calculateBonuses(standard_program, 9999) == mul(1, 0.5)
    assert calculateBonuses(standard_program, 10000) == mul(1.5, 0.5)
    assert calculateBonuses(standard_program, 10001) == mul(1.5, 0.5)
    assert calculateBonuses(standard_program, 49999) == mul(1.5, 0.5)
    assert calculateBonuses(standard_program, 50000) == mul(2, 0.5)
    assert calculateBonuses(standard_program, 50001) == mul(2, 0.5)
    assert calculateBonuses(standard_program, 99999) == mul(2, 0.5)
    assert calculateBonuses(standard_program, 100000) == mul(2.5, 0.5)
    assert calculateBonuses(standard_program, 100001) == mul(2.5, 0.5)


def test_premium():
    assert calculateBonuses(premium_program, 0) == mul(1, 0.1)
    assert calculateBonuses(premium_program, 9999) == mul(1, 0.1)
    assert calculateBonuses(premium_program, 10000) == mul(1.5, 0.1)
    assert calculateBonuses(premium_program, 10001) == mul(1.5, 0.1)
    assert calculateBonuses(premium_program, 49999) == mul(1.5, 0.1)
    assert calculateBonuses(premium_program, 50000) == mul(2, 0.1)
    assert calculateBonuses(premium_program, 50001) == mul(2, 0.1)
    assert calculateBonuses(premium_program, 99999) == mul(2, 0.1)
    assert calculateBonuses(premium_program, 100000) == mul(2.5, 0.1)
    assert calculateBonuses(premium_program, 100001) == mul(2.5, 0.1)


def test_diamond():
    assert calculateBonuses(diamond_program, 0) == mul(1, 0.2)
    assert calculateBonuses(diamond_program, 9999) == mul(1, 0.2)
    assert calculateBonuses(diamond_program, 10000) == mul(1.5, 0.2)
    assert calculateBonuses(diamond_program, 10001) == mul(1.5, 0.2)
    assert calculateBonuses(diamond_program, 49999) == mul(1.5, 0.2)
    assert calculateBonuses(diamond_program, 50000) == mul(2, 0.2)
    assert calculateBonuses(diamond_program, 50001) == mul(2, 0.2)
    assert calculateBonuses(diamond_program, 99999) == mul(2, 0.2)
    assert calculateBonuses(diamond_program, 100000) == mul(2.5, 0.2)
    assert calculateBonuses(diamond_program, 100001) == mul(2.5, 0.2)


def test_bla_bla():
    assert calculateBonuses(no_program, 0) == mul(1, 0.0)
    assert calculateBonuses(no_program, 9999) == mul(1, 0.0)
    assert calculateBonuses(no_program, 10000) == mul(1.5, 0.0)
    assert calculateBonuses(no_program, 10001) == mul(1.5, 0.0)
    assert calculateBonuses(no_program, 49999) == mul(1.5, 0.0)
    assert calculateBonuses(no_program, 50000) == mul(2, 0.0)
    assert calculateBonuses(no_program, 50001) == mul(2, 0.0)
    assert calculateBonuses(no_program, 99999) == mul(2, 0.0)
    assert calculateBonuses(no_program, 100000) == mul(2.5, 0.0)
    assert calculateBonuses(no_program, 100001) == mul(2.5, 0.0)


def test_nothing():
    assert calculateBonuses(nothing_program, 0) == mul(1, 0.0)
    assert calculateBonuses(nothing_program, 9999) == mul(1, 0.0)
    assert calculateBonuses(nothing_program, 10000) == mul(1.5, 0.0)
    assert calculateBonuses(nothing_program, 10001) == mul(1.5, 0.0)
    assert calculateBonuses(nothing_program, 49999) == mul(1.5, 0.0)
    assert calculateBonuses(nothing_program, 50000) == mul(2, 0.0)
    assert calculateBonuses(nothing_program, 50001) == mul(2, 0.0)
    assert calculateBonuses(nothing_program, 99999) == mul(2, 0.0)
    assert calculateBonuses(nothing_program, 100000) == mul(2.5, 0.0)
    assert calculateBonuses(nothing_program, 100001) == mul(2.5, 0.0)


def mul(a, b):
    return a * b
